const router = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('./User');
const { SECRET } = require('./enviroment');

router.get('/login', (req, res) =>
  res.send('ltauth'));

router.post('/login', passport.authenticate('local', { session: false }), (req, res) =>
  res.json({ token: jwt.sign(req.user.toJSON(), SECRET) }));

router.post('/register', (req, res) => {
  (new User(req.body)).save((err, user) => {
    if (err) return res.json({ message: err.message });
    return res.json(user);
  });
});

router.get('/me', passport.authenticate('jwt', { session: false }), (req, res) => {
  res.json(req.user);
});

module.exports = router;
