const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = mongoose.Schema({
  email: String,
  password: String,
});

UserSchema.pre('save', function save(next) {
  const self = this;
  mongoose.models.User.findOne({ email: self.email }, (err, user) => {
    if (err) return next(err);
    if (user) {
      self.invalidate('email', 'Email must be unique');
      next(new Error('Email must be unique'));
    }
    if (!self.isModified('password') && !self.isNew) return next();
    return bcrypt.hash(self.password, 10, (hashErr, hash) => {
      if (hashErr) return next(hashErr);
      self.password = hash;
      return next();
    });
  });
});

UserSchema.methods.verifyPassword = function verifyPassword(password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
