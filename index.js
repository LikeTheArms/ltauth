const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const passportJWT = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const bodyParser = require('body-parser');
const User = require('./User');
const { SECRET, DB, PORT } = require('./enviroment');

const app = express();
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

const localopts = {
  usernameField: 'email',
};

const jwtopts = {
  secretOrKey: SECRET,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

mongoose.connect(DB);

passport.use(new LocalStrategy(localopts, (email, password, done) => {
  User.findOne({ email }, (err, user) => {
    if (err) return done(err);
    if (!user) return done(null, false);
    if (!user.verifyPassword(password)) return done(null, false);
    return done(null, user);
  });
}));

passport.use(new JWTStrategy(jwtopts, (payload, done) => {
  const { _id } = payload;
  User.findOne({ _id }, (err, user) => {
    if (err) return done(err, false);
    if (!user) return done(null, false);
    return done(null, user);
  });
}));

app.use(bodyParser.json());
app.use(passport.initialize());

app.use('/', require('./route.js'));

app.listen(PORT, () => console.log(`Listen: ${PORT}`)); // eslint-disable-line no-console
